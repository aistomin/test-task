package com.istomin.test;

import org.jmock.Mockery;

import java.util.Random;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:55 AM
 * <p/>
 * Base test class with necessary fields to avoid fields duplication
 */
public abstract class BaseTest {

    protected Random random = new Random();
    protected Mockery mockery = new Mockery();

}
