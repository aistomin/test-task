package com.istomin.service;

import com.istomin.dao.PersonDAO;
import com.istomin.dao.PersonSearchFilter;
import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;
import com.istomin.test.BaseTest;
import com.istomin.validators.PersonValidator;
import junit.framework.Assert;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:54 AM
 */
public class PersonServiceImplTest extends BaseTest {

    private PersonServiceImpl service;
    private PersonDAO dao;
    private PersonValidator validator;

    @Before
    public void setUp() throws Exception {
        dao = mockery.mock(PersonDAO.class);
        validator = mockery.mock(PersonValidator.class);

        service = new PersonServiceImpl();
        service.setDao(dao);
        service.setValidator(validator);
    }

    @Test
    public void testCreateSuccess() throws Exception {

        final Person person = createPerson();

        mockery.checking(new Expectations() {{
            oneOf(validator).validate(person);
            oneOf(dao).create(person);
        }});

        service.create(person);

        mockery.assertIsSatisfied();
    }

    @Test(expected = PersonValidationException.class)
    public void testCreateValidationError() throws Exception {

        final Person person = createPerson();

        mockery.checking(new Expectations() {{
            oneOf(validator).validate(person);
            will(throwException(new PersonValidationException("Validation Error!!!")));
        }});

        service.create(person);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateNullPerson() throws Exception {

        Person person = null;

        service.create(person);
    }

    @Test
    public void testDelete() throws Exception {

        final Person person = createPerson();

        mockery.checking(new Expectations() {{
            oneOf(dao).delete(person);
        }});

        service.delete(person);

        mockery.assertIsSatisfied();
    }

    @Test(expected = NullPointerException.class)
    public void testDeleteNullPerson() throws Exception {

        final Person person = null;

        service.delete(person);
    }

    @Test
    public void testUpdateSuccess() throws Exception {

        final Person person = createPerson();

        mockery.checking(new Expectations() {{
            oneOf(validator).validate(person);
            oneOf(dao).update(person);
        }});

        service.update(person);

        mockery.assertIsSatisfied();
    }

    @Test(expected = PersonValidationException.class)
    public void testUpdateValidationError() throws Exception {

        final Person person = createPerson();

        mockery.checking(new Expectations() {{
            oneOf(validator).validate(person);
            will(throwException(new PersonValidationException("Validation Error!!!")));
        }});

        service.update(person);
    }

    @Test(expected = NullPointerException.class)
    public void testUpdateNullPerson() throws Exception {

        Person person = null;

        service.update(person);
    }

    @Test
    public void testSearch() throws Exception {

        final List<Person> expectedResult = new ArrayList<Person>();

        for (int i = 0; i < random.nextInt(9); i++)
        {
            expectedResult.add(createPerson());
        }

        mockery.checking(new Expectations(){{
            oneOf(dao).searchPersons(with(any(PersonSearchFilter.class)));
            will(returnValue(expectedResult));
        }});

        List<Person> persons = service.searchPersons(random.nextInt(), "FirstName" + random.nextInt(), "LastName" + random.nextInt(), random.nextInt(100));

        Assert.assertEquals(expectedResult.size(), persons.size());

        mockery.assertIsSatisfied();
    }

    @Test
    public void testloadAllPersons() throws Exception {

        final List<Person> expectedResult = new ArrayList<Person>();

        for (int i = 0; i < random.nextInt(9); i++)
        {
            expectedResult.add(createPerson());
        }

        mockery.checking(new Expectations(){{
            oneOf(dao).searchPersons(with(any(PersonSearchFilter.class)));
            will(returnValue(expectedResult));
        }});

        List<Person> persons = service.loadAllPersons();

        Assert.assertEquals(expectedResult.size(), persons.size());

        mockery.assertIsSatisfied();
    }


    private Person createPerson() {
        Person person = new Person();
        person.setFirstName("FirstName" + random.nextInt());
        person.setLastName("LastName" + random.nextInt());
        person.setAge(random.nextInt(100));
        return person;
    }
}
