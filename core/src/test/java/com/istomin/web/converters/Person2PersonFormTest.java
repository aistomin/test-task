package com.istomin.web.converters;

import com.istomin.entities.Person;
import com.istomin.test.BaseTest;
import com.istomin.web.forms.PersonForm;
import junit.framework.Assert;
import org.junit.Test;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 10:04 PM
 */
public class Person2PersonFormTest extends BaseTest {

    @Test
    public void testConvertPersonForm() throws Exception {

        int expectedId = random.nextInt();
        String expectedFirstName = "FirstName" + random.nextInt();
        String expectedLastName = "LastName" + random.nextInt();
        int expectedAge = random.nextInt();

        PersonForm personForm = new PersonForm();
        personForm.setId(expectedId);
        personForm.setFirstName(expectedFirstName);
        personForm.setLastName(expectedLastName);
        personForm.setAge(expectedAge);

        Person person = Person2PersonForm.convert(personForm);

        Assert.assertEquals(expectedId, person.getId());
        Assert.assertEquals(expectedFirstName, person.getFirstName());
        Assert.assertEquals(expectedLastName, person.getLastName());
        Assert.assertEquals(expectedAge, person.getAge());
    }

    @Test
    public void testConvertPerson() throws Exception {
        int expectedId = random.nextInt();
        String expectedFirstName = "FirstName" + random.nextInt();
        String expectedLastName = "LastName" + random.nextInt();
        int expectedAge = random.nextInt();

        Person person = new Person();
        person.setId(expectedId);
        person.setFirstName(expectedFirstName);
        person.setLastName(expectedLastName);
        person.setAge(expectedAge);

        PersonForm personForm = Person2PersonForm.convert(person);

        Assert.assertEquals(expectedId, personForm.getId());
        Assert.assertEquals(expectedFirstName, personForm.getFirstName());
        Assert.assertEquals(expectedLastName, personForm.getLastName());
        Assert.assertEquals(expectedAge, personForm.getAge());
    }
}
