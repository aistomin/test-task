package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.service.PersonService;
import com.istomin.test.BaseTest;
import com.istomin.web.constants.ErrorMessages;
import com.istomin.web.constants.IndexPageConstants;
import com.istomin.web.forms.SearchForm;
import junit.framework.Assert;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 10:10 PM
 */
public class IndexControllerTest extends BaseTest {

    private ApplicationContext context;
    private PersonService personService;
    private IndexController controller;

    @Before
    public void setUp() throws Exception {

        context = mockery.mock(ApplicationContext.class);
        personService = mockery.mock(PersonService.class);

        controller = new IndexController();
        controller.setContext(context);
    }

    @Test
    public void testIndex() throws Exception {

        final List<Person> expectedPersons = new ArrayList<>();

        mockery.checking(new Expectations() {{
            oneOf(context).getBean(PersonService.class);
            will(returnValue(personService));

            oneOf(personService).loadAllPersons();
            will(returnValue(expectedPersons));
        }});

        ModelAndView modelAndView = controller.index();

        Assert.assertEquals(expectedPersons, modelAndView.getModel().get(IndexPageConstants.PERSONS));

        SearchForm searchForm = (SearchForm) modelAndView.getModel().get(IndexPageConstants.SEARCH_FORM);

        Assert.assertNull(searchForm.getId());
        Assert.assertNull(searchForm.getFirstName());
        Assert.assertNull(searchForm.getLastName());
        Assert.assertNull(searchForm.getAge());

        mockery.assertIsSatisfied();
    }

    @Test
    public void testIndexError() throws Exception {

        mockery.checking(new Expectations() {{
            oneOf(context).getBean(PersonService.class);
            will(returnValue(personService));

            oneOf(personService).loadAllPersons();
            will(throwException(new RuntimeException("Error" + random.nextInt())));
        }});

        ModelAndView modelAndView = controller.index();

        Assert.assertEquals(ErrorMessages.ERROR_LOAD_PERSONS, modelAndView.getModel().get(IndexPageConstants.ERROR));

        SearchForm searchForm = (SearchForm) modelAndView.getModel().get(IndexPageConstants.SEARCH_FORM);

        Assert.assertNull(searchForm.getId());
        Assert.assertNull(searchForm.getFirstName());
        Assert.assertNull(searchForm.getLastName());
        Assert.assertNull(searchForm.getAge());

        mockery.assertIsSatisfied();
    }
}
