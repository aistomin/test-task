package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.service.PersonService;
import com.istomin.test.BaseTest;
import com.istomin.web.constants.ErrorMessages;
import com.istomin.web.constants.IndexPageConstants;
import com.istomin.web.forms.SearchForm;
import junit.framework.Assert;
import org.jmock.Expectations;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.util.ArrayList;
import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 10:21 PM
 */
public class SearchControllerTest extends BaseTest {

    private SearchController controller;
    private ApplicationContext context;
    private PersonService personService;

    @Before
    public void setUp() throws Exception {
        context = mockery.mock(ApplicationContext.class);
        personService = mockery.mock(PersonService.class);

        controller = new SearchController();
        controller.setContext(context);
    }

    @Test
    public void testSearch() throws Exception {

        final int expectedId = random.nextInt();
        final String expectedFirstName = "FirstName" + random.nextInt();
        final String expectedLastName = "LastName" + random.nextInt();
        final int expectedAge = random.nextInt(100);

        final List<Person> expectedPersons = new ArrayList<>();

        SearchForm searchForm = new SearchForm();
        searchForm.setId(expectedId);
        searchForm.setFirstName(expectedFirstName);
        searchForm.setLastName(expectedLastName);
        searchForm.setAge(expectedAge);

        mockery.checking(new Expectations(){{
            oneOf(context).getBean(PersonService.class);
            will(returnValue(personService));

            oneOf(personService).searchPersons(expectedId, expectedFirstName, expectedLastName, expectedAge);
            will(returnValue(expectedPersons));
        }});

        ModelAndView modelAndView = controller.search(new ExtendedModelMap(), searchForm, new BeanPropertyBindingResult("", ""), new RedirectAttributesModelMap());

        Assert.assertEquals(expectedPersons, modelAndView.getModel().get(IndexPageConstants.PERSONS));
        Assert.assertEquals(searchForm, modelAndView.getModel().get(IndexPageConstants.SEARCH_FORM));

        mockery.assertIsSatisfied();
    }

    @Test
    public void testSearchError() throws Exception {

        final int expectedId = random.nextInt();
        final String expectedFirstName = "FirstName" + random.nextInt();
        final String expectedLastName = "LastName" + random.nextInt();
        final int expectedAge = random.nextInt(100);

        SearchForm searchForm = new SearchForm();
        searchForm.setId(expectedId);
        searchForm.setFirstName(expectedFirstName);
        searchForm.setLastName(expectedLastName);
        searchForm.setAge(expectedAge);

        mockery.checking(new Expectations(){{
            oneOf(context).getBean(PersonService.class);
            will(returnValue(personService));

            oneOf(personService).searchPersons(expectedId, expectedFirstName, expectedLastName, expectedAge);
            will(throwException(new RuntimeException()));
        }});

        ModelAndView modelAndView = controller.search(new ExtendedModelMap(), searchForm, new BeanPropertyBindingResult("", ""), new RedirectAttributesModelMap());

        Assert.assertEquals(ErrorMessages.ERROR_LOAD_PERSONS, modelAndView.getModel().get(IndexPageConstants.ERROR));
        Assert.assertEquals(searchForm, modelAndView.getModel().get(IndexPageConstants.SEARCH_FORM));

        mockery.assertIsSatisfied();
    }
}
