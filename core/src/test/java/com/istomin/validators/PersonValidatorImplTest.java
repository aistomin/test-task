package com.istomin.validators;

import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;
import com.istomin.test.BaseTest;
import org.junit.Test;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 2:01 PM
 */
public class PersonValidatorImplTest extends BaseTest{

    private PersonValidatorImpl validator = new PersonValidatorImpl();

    @Test
    public void testValidateSuccess() throws Exception {

        Person person = createValidPerson();
        validator.validate(person);
    }

    @Test(expected = PersonValidationException.class)
    public void testValidateInvalidFirstName() throws Exception {

        Person person = createValidPerson();
        person.setFirstName("");

        validator.validate(person);
    }

    @Test(expected = PersonValidationException.class)
    public void testValidateInvalidLastName() throws Exception {

        Person person = createValidPerson();
        person.setLastName("");

        validator.validate(person);
    }

    @Test(expected = PersonValidationException.class)
    public void testValidateInvalidAge() throws Exception {

        Person person = createValidPerson();
        person.setAge(0);

        validator.validate(person);
    }

    private Person createValidPerson() {

        Person person = new Person();
        person.setFirstName("FirstName" + random.nextInt());
        person.setLastName("LastName" + random.nextInt());
        person.setAge(1 + random.nextInt(100));
        return person;
    }
}
