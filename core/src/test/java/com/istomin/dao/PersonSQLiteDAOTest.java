package com.istomin.dao;

import com.istomin.entities.Person;
import com.istomin.test.BaseTest;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/22/13
 * Time: 11:48 PM
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:testContext.xml")
public class PersonSQLiteDAOTest extends BaseTest{

    @Autowired
    public PersonSQLiteDAO dao;

    @Before
    public void setUp() throws Exception {

        clearDatabase();
    }

    @Test
    public void testCreate() throws Exception {

        String expectedName = "FirstName" + random.nextInt();
        String expectedLastName = "LastName" + random.nextInt();
        int expectedAge = 1 + random.nextInt(100);

        Person person = new Person();
        person.setFirstName(expectedName);
        person.setLastName(expectedLastName);
        person.setAge(expectedAge);

        dao.create(person);

        PersonSearchFilter filter = new PersonSearchFilter();
        filter.setLastName(expectedLastName);

        List<Person> persons = dao.searchPersons(filter);

        Assert.assertEquals(1, persons.size());

        Assert.assertEquals(expectedLastName, persons.get(0).getLastName());
        Assert.assertEquals(expectedName, persons.get(0).getFirstName());
        Assert.assertEquals(expectedAge, persons.get(0).getAge());
    }

    @Test
    public void testDelete() throws Exception {

        // create person and save them
        String personsLastName = "LastName" + random.nextInt();

        Person person = new Person();
        person.setFirstName("FirstName" + random.nextInt());
        person.setLastName(personsLastName);
        person.setAge(1 + random.nextInt(100));

        dao.create(person);

        PersonSearchFilter filter = new PersonSearchFilter();
        filter.setLastName(personsLastName);

        List<Person> persons = dao.searchPersons(filter);

        Assert.assertEquals(1, persons.size());

        // delete person
        dao.delete(persons.get(0));

        // check that person was deleted
        persons = dao.searchPersons(filter);

        Assert.assertEquals(0, persons.size());
    }

    @Test
    public void testUpdate() throws Exception {

        // create person with some first name
        String personsFirstNameBeforeEditing = "FirstName" + random.nextInt();
        String personsLast = "LastName" + random.nextInt();
        int expectedAge = 1 + random.nextInt(100);

        Person person = new Person();
        person.setFirstName(personsFirstNameBeforeEditing);
        person.setLastName(personsLast);
        person.setAge(expectedAge);

        dao.create(person);

        PersonSearchFilter filter = new PersonSearchFilter();
        filter.setLastName(personsLast);

        List<Person> persons = dao.searchPersons(filter);

        Assert.assertEquals(1, persons.size());

        Person savedPerson = persons.get(0);
        Assert.assertEquals(personsFirstNameBeforeEditing, savedPerson.getFirstName());

        // edit person
        String personsFirstNameAfterEditing = "AnotherFirstName" + random.nextInt();
        savedPerson.setFirstName(personsFirstNameAfterEditing);

        dao.update(savedPerson);

        // check that person was edited
        persons = dao.searchPersons(filter);

        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(personsFirstNameAfterEditing, persons.get(0).getFirstName());
    }

    @Test
    public void testSearchPersons() throws Exception
    {
        Person person0 = new Person();
        person0.setLastName("A" + random.nextInt());
        person0.setFirstName("A" + random.nextInt());
        person0.setAge(1 + random.nextInt(100));

        Person person1 = new Person();
        person1.setLastName("B" + random.nextInt());
        person1.setFirstName("A" + random.nextInt());
        person1.setAge(1 + random.nextInt(100));

        Person person2 = new Person();
        person2.setLastName(person1.getLastName());
        person2.setFirstName("B" + random.nextInt());
        person2.setAge(1 + random.nextInt(100));

        Integer person2Id = dao.create(person2);
        Integer person1Id = dao.create(person1);
        Integer person0Id = dao.create(person0);

        List<Person> persons = dao.searchPersons(new PersonSearchFilter());//search all

        // check that order is correct
        Assert.assertEquals(3, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());
        Assert.assertEquals(person1.getLastName(), persons.get(1).getLastName());
        Assert.assertEquals(person2.getLastName(), persons.get(2).getLastName());

        // search person0 by id
        PersonSearchFilter idFilter = new PersonSearchFilter();
        idFilter.setId(person0Id);

        persons = dao.searchPersons(idFilter);
        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());

        // search person0 by last name
        PersonSearchFilter lastNameFilter = new PersonSearchFilter();
        lastNameFilter.setLastName(person0.getLastName());

        persons = dao.searchPersons(lastNameFilter);
        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());

        // search person0 by first name
        PersonSearchFilter firstNameFilter = new PersonSearchFilter();
        firstNameFilter.setFirstName(person0.getFirstName());

        persons = dao.searchPersons(firstNameFilter);
        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());

        // search person0 by age
        PersonSearchFilter ageFilter = new PersonSearchFilter();
        ageFilter.setAge(person0.getAge());

        persons = dao.searchPersons(ageFilter);
        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());

        // search person2 by all parameters
        PersonSearchFilter allParametersFilter = new PersonSearchFilter();
        allParametersFilter.setId(person0Id);
        allParametersFilter.setFirstName(person0.getFirstName());
        allParametersFilter.setLastName(person0.getLastName());
        allParametersFilter.setAge(person0.getAge());

        persons = dao.searchPersons(allParametersFilter);
        Assert.assertEquals(1, persons.size());
        Assert.assertEquals(person0.getLastName(), persons.get(0).getLastName());

        // search not existed person
        PersonSearchFilter notExistedLastNameFilter = new PersonSearchFilter();
        notExistedLastNameFilter.setLastName("Z" + random.nextInt());

        persons = dao.searchPersons(notExistedLastNameFilter);
        Assert.assertEquals(0, persons.size());
    }

    private void clearDatabase() {

        List<Person> persons = dao.searchPersons(new PersonSearchFilter());

        for (Person person : persons) {
            dao.delete(person);
        }
    }
}
