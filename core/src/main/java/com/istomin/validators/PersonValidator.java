package com.istomin.validators;

import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:49 AM
 * <p/>
 * Interface of validator that validates person entity before it processing
 */
public interface PersonValidator {
    /**
     * Validation method
     *
     * @param person - person's entity that needs to be validated
     */
    void validate(Person person) throws PersonValidationException;
}
