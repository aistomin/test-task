package com.istomin.validators;

import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;
import org.apache.commons.lang.StringUtils;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 2:01 PM
 */
public class PersonValidatorImpl implements PersonValidator {
    @Override
    public void validate(Person person) throws PersonValidationException {

        if (StringUtils.isEmpty(person.getFirstName())) {
            throw new PersonValidationException("Person's first name can not be empty.");
        }

        if (StringUtils.isEmpty(person.getLastName())) {
            throw new PersonValidationException("Person's last name can not be empty.");
        }

        if(person.getAge() <= 0) // 0 years old baby is too young to be a person :)
        {
           throw new PersonValidationException("Person's age is always positive number.");
        }
    }
}
