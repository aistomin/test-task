package com.istomin.service;

import com.istomin.dao.PersonSearchFilter;
import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:44 AM
 * <p/>
 * Interface of service that provides logic of punter's modifications and encapsulates all core logic
 */
public interface PersonService {
    /**
     * Create the person
     *
     * @param person - person's entity that needs to be registered in system
     */
    public void create(Person person) throws PersonValidationException;

    /**
     * Delete the person
     *
     * @param person - person that needs to be deleted from system
     */
    public void delete(Person person);

    /**
     * Update person's data
     *
     * @param person - new person's data that needs to be updated in system
     */
    public void update(Person person) throws PersonValidationException;

    /**
     * Search all persons with specified filter parameters. If don't want to search by any of parameters so just put null into parameter
     *
     * @return - list of persons with sought-for parameters
     */
    public List<Person> searchPersons(Integer id, String firstName, String lastName, Integer age);

    /**
     * Load all persons from database
     *
     * @return persons' list
     */
    List<Person> loadAllPersons();

    /**
     * Load person by it's unique identifier
     *
     * @param id - punter's unique identifier
     *
     * @return person
     */
    Person getPersonById(int id);
}
