package com.istomin.service;

import com.istomin.dao.PersonDAO;
import com.istomin.dao.PersonSearchFilter;
import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;
import com.istomin.validators.PersonValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Required;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:53 AM
 */
public class PersonServiceImpl implements PersonService {

    private Logger logger = LoggerFactory.getLogger(getClass());

    private PersonDAO dao;
    private PersonValidator validator;

    @Required
    public void setDao(PersonDAO dao) {
        this.dao = dao;
    }

    @Required
    public void setValidator(PersonValidator validator) {
        this.validator = validator;
    }

    @Override
    public void create(Person person) throws PersonValidationException {

        if (person == null) {
            throw new NullPointerException("Can not create person. The object for saving is null.");
        }

        logger.debug("Person with last name {} creation. Validation.....", person.getLastName());
        validator.validate(person);

        logger.debug("Person with last name {} succesfully validated. Saving to database.....", person.getLastName());
        dao.create(person);

        logger.info("Person with last name {} succesfully created.", person.getLastName());
    }

    @Override
    public void delete(Person person) {

        if (person == null) {
            throw new NullPointerException("Can not delete person. The object for deleting is null.");
        }

        logger.debug("Delete person with id = {} from database.....", person.getId());
        dao.delete(person);

        logger.info("Person with id = {} succesfully deleted.", person.getId());
    }

    @Override
    public void update(Person person) throws PersonValidationException {

        if (person == null) {
            throw new NullPointerException("Can not update person. The object for saving is null.");
        }

        logger.debug("Person with id = {} update. Validation.....", person.getId());
        validator.validate(person);

        logger.debug("Person with id = {} succesfully validated. Saving to database.....", person.getId());
        dao.update(person);

        logger.info("Person with id = {} succesfully updated.", person.getId());
    }

    @Override
    public List<Person> searchPersons(Integer id, String firstName, String lastName, Integer age) {

        logger.debug("Search persons by parameters: {}, {}, {}, {} .....", new Object[]{id, firstName, lastName, age});

        List<Person> persons = dao.searchPersons(createFilter(id, firstName, lastName, age));

        logger.info("{} persons found.", persons.size());

        return persons;
    }

    @Override
    public List<Person> loadAllPersons() {
        // all persons without filtration
        return searchPersons(null, null, null, null);
    }

    @Override
    public Person getPersonById(int id) {

        List<Person> persons = searchPersons(id, null, null, null);
        return persons.size() > 0 ? persons.get(0) : null;
    }

    private PersonSearchFilter createFilter(Integer id, String firstName, String lastName, Integer age) {

        PersonSearchFilter filter = new PersonSearchFilter();
        filter.setId(id);
        filter.setFirstName(firstName);
        filter.setLastName(lastName);
        filter.setAge(age);
        return filter;
    }
}
