package com.istomin.dao;

import com.istomin.entities.Person;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/22/13
 * Time: 10:40 PM
 * <p/>
 * Interface of data access object that is CRUD for entity Person
 */
public interface PersonDAO {

    /**
     * Create the person
     *
     * @param person - data that needs to be saved in storage
     * @return unique identifier of created record
     */
    public Integer create(Person person);

    /**
     * Delete the person
     *
     * @param person - person that needs to be deleted in storage
     */
    public void delete(Person person);

    /**
     * Update person's data
     *
     * @param person - new person's data that needs to be saved in storage
     */
    public void update(Person person);

    /**
     * Search all persons with specified filter parameters
     *
     * @param filter - person's filter parameters. If some of fields is null - search will be no filtered by this fields
     * @return - list of persons with sought-for parameters
     */
    public List<Person> searchPersons(PersonSearchFilter filter);
}
