package com.istomin.dao;

import com.istomin.entities.Person;
import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.orm.hibernate3.HibernateTemplate;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/22/13
 * Time: 10:45 PM
 */
public class PersonSQLiteDAO implements PersonDAO {

    private HibernateTemplate hibernateTemplate;

    @Required
    public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
        this.hibernateTemplate = hibernateTemplate;
    }

    @Override
    public Integer create(Person person) {
        return (Integer) hibernateTemplate.save(person);
    }

    @Override
    public void delete(Person person) {
        hibernateTemplate.delete(person);
    }

    @Override
    public void update(Person person) {
        hibernateTemplate.update(person);
    }

    @Override
    public List<Person> searchPersons(PersonSearchFilter filter) {

        DetachedCriteria criteria = DetachedCriteria.forClass(Person.class);

        if(filter.getId() != null)
        {
            criteria.add(Restrictions.eq("id", filter.getId()));
        }

        if(!StringUtils.isEmpty(filter.getFirstName()))
        {
            criteria.add(Restrictions.eq("firstName", filter.getFirstName()).ignoreCase());
        }

        if(!StringUtils.isEmpty(filter.getLastName()))
        {
            criteria.add(Restrictions.eq("lastName", filter.getLastName()).ignoreCase());
        }

        if(filter.getAge() != null)
        {
            criteria.add(Restrictions.eq("age", filter.getAge()));
        }

        criteria.addOrder(Order.asc("lastName"));
        criteria.addOrder(Order.asc("firstName"));

        return hibernateTemplate.findByCriteria(criteria);
    }
}
