package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.service.PersonService;
import com.istomin.web.constants.IndexPageConstants;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * User: aistomin
 * Date: 8/26/13
 * Time: 6:34 PM
 */
@Controller
public class DeleteController extends AbstractController{
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(int  id) {

        try {

            deletePerson(id);

            return new ModelAndView(IndexPageConstants.REDIRECT_TO_ROOT);

        } catch (Exception e) {

            logger.error("Error occurred while deleting person.", e);
            return new ModelAndView("redirect:/", IndexPageConstants.ERROR, e.getMessage());
        }
    }

    private void deletePerson(int personId) {

        logger.debug("Delete person with id = {}", personId);

        PersonService personService = context.getBean(PersonService.class);

        Person person = personService.getPersonById(personId);

        if(person == null)
        {
            throw new IllegalStateException(String.format("Person with id = %d does not exist.", personId));
        }

        personService.delete(person);

        logger.debug("Person successfully deleted.");
    }
}
