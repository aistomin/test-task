package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.service.PersonService;
import com.istomin.web.constants.ErrorMessages;
import com.istomin.web.constants.IndexPageConstants;
import com.istomin.web.forms.SearchForm;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 3:27 PM
 */
@Controller
public class IndexController extends AbstractController{

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index() {

        try {

            logger.debug("Get PersonService from context.....");
            PersonService personService = context.getBean(PersonService.class);

            logger.debug("PersonService from context is {}. Loading persons.....", personService);
            List<Person> persons = personService.loadAllPersons();
            logger.debug("{} persons have been loaded.", persons.size());

            Map<String, Object> model = new HashMap<>();
            model.put(IndexPageConstants.PERSONS, persons);
            model.put(IndexPageConstants.SEARCH_FORM, new SearchForm());

            return new ModelAndView(IndexPageConstants.NAME, model);
        } catch (Exception e) {

            logger.error(ErrorMessages.ERROR_LOAD_PERSONS, e);

            Map<String, Object> model = new HashMap<>();
            model.put(IndexPageConstants.ERROR, ErrorMessages.ERROR_LOAD_PERSONS);
            model.put(IndexPageConstants.SEARCH_FORM, new SearchForm());

            return new ModelAndView(IndexPageConstants.NAME, model);
        }
    }




}
