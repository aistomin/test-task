package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.exceptions.validation.PersonValidationException;
import com.istomin.service.PersonService;
import com.istomin.web.constants.PersonPageConstants;
import com.istomin.web.converters.Person2PersonForm;
import com.istomin.web.forms.PersonForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 9:57 PM
 */
@Controller
public class PersonEditController extends AbstractController {

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {

        return new ModelAndView(PersonPageConstants.NAME, PersonPageConstants.FORM, new PersonForm());
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(int id) {

        try {

            logger.debug("Load person by id = {}.....", id);
            PersonService personService = context.getBean(PersonService.class);
            Person person = personService.getPersonById(id);
            logger.debug("Loaded person by id = {} is {}", person);

            return new ModelAndView(PersonPageConstants.NAME, PersonPageConstants.FORM, Person2PersonForm.convert(person));

        } catch (Exception e) {

            logger.error("Error occurred while loading person by id = {} from server.", id, e);

            return new ModelAndView(PersonPageConstants.NAME, PersonPageConstants.ERROR, e.getMessage());
        }
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String create(Model model, PersonForm personForm, BindingResult result, RedirectAttributes redirectAttributes) {

        try {

            savePerson(personForm);

            return "redirect:/";

        } catch (Exception e) {

            logger.error("Error occurred while saving person.", e);

            model.addAttribute(PersonPageConstants.ERROR, e.getMessage());

            return PersonPageConstants.NAME;
        }
    }

    private void savePerson(PersonForm personForm) throws PersonValidationException {

        logger.debug("Saving person with id = {}", personForm.getId());

        Person person = Person2PersonForm.convert(personForm);
        PersonService personService = context.getBean(PersonService.class);

        if (person.getId() > 0) {
            personService.update(person);

        } else {

            personService.create(person);
        }

        logger.debug("Person with successfully saved.");
    }
}
