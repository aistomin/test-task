package com.istomin.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 9:54 PM
 */
public class AbstractController {

    protected Logger logger = LoggerFactory.getLogger(getClass());

    protected ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }
}
