package com.istomin.web.controllers;

import com.istomin.entities.Person;
import com.istomin.service.PersonService;
import com.istomin.web.constants.ErrorMessages;
import com.istomin.web.constants.IndexPageConstants;
import com.istomin.web.forms.SearchForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 9:53 PM
 */
@Controller
public class SearchController extends AbstractController{

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ModelAndView search(Model model, SearchForm searchForm, BindingResult result, RedirectAttributes redirectAttributes) {

        try {

            logger.debug("Search person.....");

            PersonService personService = context.getBean(PersonService.class);

            List<Person> persons = personService.searchPersons(searchForm.getId(), searchForm.getFirstName(), searchForm.getLastName(), searchForm.getAge());

            logger.debug("{} persons found.", persons.size());

            model.addAttribute(IndexPageConstants.PERSONS, persons);
            model.addAttribute(IndexPageConstants.SEARCH_FORM, searchForm);

            return new ModelAndView(IndexPageConstants.NAME, model.asMap());

        } catch (Exception e) {

            logger.error(ErrorMessages.ERROR_LOAD_PERSONS, e);

            model.addAttribute(IndexPageConstants.ERROR, ErrorMessages.ERROR_LOAD_PERSONS);
            model.addAttribute(IndexPageConstants.SEARCH_FORM, searchForm);

            return new ModelAndView(IndexPageConstants.NAME, model.asMap());
        }
    }
}
