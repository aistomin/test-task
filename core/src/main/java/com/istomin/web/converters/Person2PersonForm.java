package com.istomin.web.converters;

import com.istomin.entities.Person;
import com.istomin.web.forms.PersonForm;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 6:37 PM
 */
public class Person2PersonForm {

    public static PersonForm convert(Person person) {

        PersonForm personForm = new PersonForm();
        personForm.setId(person.getId());
        personForm.setFirstName(person.getFirstName());
        personForm.setLastName(person.getLastName());
        personForm.setAge(person.getAge());
        return personForm;
    }

    public static Person convert(PersonForm personForm) {

        Person person = new Person();
        person.setId(personForm.getId());
        person.setFirstName(personForm.getFirstName());
        person.setLastName(personForm.getLastName());
        person.setAge(personForm.getAge());
        return person;
    }

}
