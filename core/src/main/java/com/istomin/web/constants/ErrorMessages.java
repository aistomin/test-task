package com.istomin.web.constants;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 10:19 PM
 */
public class ErrorMessages {
    public static final String ERROR_LOAD_PERSONS = "Error occurred while loading persons from server.";
}
