package com.istomin.web.constants;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 3:57 PM
 *
 * Constants of web index page
 */
public class IndexPageConstants {
    /**
     * Index page view name
     */
    public final static String NAME = "index";

    /**
     * Error variable name
     */
    public final static String ERROR = "error";

    /**
     * Persons list variable name
     */
    public final static String PERSONS = "persons";

    /**
     * Filter parameters
     */
    public final static String SEARCH_FORM = "searchForm";

    /**
     * Redirect to site's root url
     */
    public final static String REDIRECT_TO_ROOT = "redirect:/";

}
