package com.istomin.web.constants;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 6:28 PM
 */
public class PersonPageConstants {
    public final static String NAME = "person";
    public final static String FORM = "personForm";
    public final static String ERROR = "error";
}
