package com.istomin.exceptions.validation;

/**
 * User: aistomin
 * Date: 8/24/13
 * Time: 11:50 AM
 *
 * Exception that throws when person's data is invalid
 */
public class PersonValidationException extends Exception {

    public PersonValidationException(String message) {
        super(message);
    }
}
