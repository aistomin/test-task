<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--
  User: aistomin
  Date: 8/24/13
  Time: 5:47 PM
--%>
<html>
<head>
    <title>Person</title>
    <link rel="stylesheet" media="all" type="text/css" href="<c:url value='/css/styles.css'/>">
</head>
<body>

<c:if test="${not empty error}">
    <p class="error_message">${error}</p>
</c:if>

<div class="centred">
    <form:form method="post" modelAttribute="personForm" action="save" id="personForm" autocomplete="off">
        <fieldset style="width: 0">
            <legend>Enter Person's Data</legend>

            <label for="field-id">Id</label>

            <div>
                <form:input path="id" id="field-id" tabindex="2" maxlength="35" placeholder="Do not try to modify"
                            readonly="true"/>
            </div>

            <label for="field-firstName">First Name</label>

            <div>
                <form:input path="firstName" id="field-firstName" tabindex="2" maxlength="35" placeholder="First Name"/>
            </div>

            <label for="field-lastName">Last Name</label>

            <div>
                <form:input path="lastName" id="field-lastName" tabindex="3" maxlength="35" placeholder="Last Name"/>
            </div>

            <label class="control-label" for="field-age">Age</label>

            <div>
                <form:input path="age" id="field-age" tabindex="4" maxlength="35" placeholder="Age"/>
            </div>

            <div class="form-actions">
                <button type="submit">Save</button>
                <button type="button" onclick="history.go(-1);">Cancel</button>
            </div>
        </fieldset>
    </form:form>
</div>
</body>
</html>