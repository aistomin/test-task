<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%--
  User: aistomin
  Date: 8/24/13
  Time: 2:51 PM
--%>
<html>
<head>
    <link rel="stylesheet" media="all" type="text/css" href="<c:url value='/css/styles.css'/>">
    <title>Persons list</title>
</head>
<body>
<div class="centred">
    <c:choose>
        <c:when test="${not empty error}">
            <p class="error_message">${error}</p>
        </c:when>

        <c:otherwise>

            <form:form method="post" modelAttribute="searchForm" action="search" id="searchForm" autocomplete="off">
                <fieldset style="width: 0">
                    <legend>Filter</legend>

                    <label for="field-id">Id</label>

                    <div>
                        <form:input path="id" id="field-id" tabindex="2" maxlength="35" placeholder="Id"/>
                    </div>

                    <label for="field-firstName">First Name</label>

                    <div>
                        <form:input path="firstName" id="field-firstName" tabindex="2" maxlength="35"
                                    placeholder="First Name"/>
                    </div>

                    <label for="field-lastName">Last Name</label>

                    <div>
                        <form:input path="lastName" id="field-lastName" tabindex="3" maxlength="35"
                                    placeholder="Last Name"/>
                    </div>

                    <label class="control-label" for="field-age">Age</label>

                    <div>
                        <form:input path="age" id="field-age" tabindex="4" maxlength="35" placeholder="Age"/>
                    </div>

                    <div class="form-actions">
                        <button type="submit">Search</button>
                    </div>
                </fieldset>
            </form:form>

            <table title="Persons list" border="1">
                <tr>
                    <th>Id</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Age</th>
                    <th>&nbsp</th>
                    <th>&nbsp</th>
                </tr>
                <c:forEach var="person" items="${persons}">
                    <tr>
                        <td>${person.id}</td>
                        <td>${person.lastName}</td>
                        <td>${person.firstName}</td>
                        <td>${person.age}</td>
                        <td><a href="edit.htm?id=${person.id}">Edit</a></td>
                        <td><a href="delete.htm?id=${person.id}">Delete</a></td>
                    </tr>
                </c:forEach>
            </table>
            <p class="centred"><a href="create.htm">Add Person</a></p>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>